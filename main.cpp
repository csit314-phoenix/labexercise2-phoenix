#include <iostream>
#include <math.h>
// using std
using namespace std;


// Completed BY AZIZUL
// Completed BY ALLIS
// Completed BY DIBBO
// Completed BY DIPA

// calling the function
double celsiusToFahrenheit(double);
double fahrenheitToCelsius(double);

double calcCircumference(double);
double areaOfCircle(double);

double calcCircumference(double);
double areaOfCircle(double);

double cylinderVolumeCalculation(double, double);
double coneVolumeCalculation(double, double);


// main function
int main()
{
    double celsiusValue;    // to store celsius value
    double fahrenheitValue;  // to store fahrenheit value
    double circumferenceValue; // to store circumference value
    double radiusValue; // to store radius value
    double areaValue; // to store area of a circle value

    int length, breadth, area;

    int option;
    float side1, side2, side3, area1, s;

    double cylinderHeight;  // to store cylinder height value
    double cylinderRadius;  // to store cylinder radius value
    double cylinderVolume;  // to store cylinder volume value
    double coneHeight;  // to store cone height value
    double coneRadius;  // to store cone radius value
    double coneVolume;  // to store cone volume value

    // main menu
    do
    {
        cout << "------------------------------------" << endl;
        cout << "WELCOME TO CALCULATIONS PROGRAM" << endl;
        cout << "------------------------------------" << endl;
        cout << "1. Convert Celsius to Fahrenheit" << endl;
        cout << "2. Convert Fahrenheit to Celsius" << endl;
        cout << "3. Calculate Circumference of a circle"<<endl;
        cout << "4. Calculate Area of a circle"<<endl;
        cout << "5. Area of Rectangle"<<endl;
        cout << "6. Area of Triangle (Heron�s Formula)"<<endl;
        cout << "7. Volume of Cylinder"<<endl;
        cout << "8. Volume of Cone"<<endl;
        cout << "0. QUIT PROGRAM" << endl;

        cout << "Please enter your option: ";
        cin >> option;
        cin.ignore(3000, '\n');

        while (option < 0 || option > 8 || cin.fail())
        {
            if (cin.fail())
            {
                cin.clear() ;
                cin.ignore(3000, '\n');
            }

            cout << "\nError! Invalid option entered. Please enter again." << endl;
            cout << "\nPlease enter your option: ";
            cin >> option;
            cin.ignore(3000, '\n');
        }

        switch(option)
        {
            // convert Celsius to Fahrenheit
            case 1:
            {
                cout << "\nConvert Celsius to Fahrenheit" << endl;
                cout << "-------------------------------" << endl;
                // receiving user input
                cout << "Please Enter a Celsius value: ";
                cin >> celsiusValue;

                // calling the function to do coversion
                fahrenheitValue = celsiusToFahrenheit(celsiusValue);
                cout << "\nFahrenheit value is : " << fahrenheitValue << "F" << endl;
            }
            break;

           // convert Fahrenheit to Celsius
            case 2:
            {
                cout << "\nConvert Fahrenheit to Celsius" << endl;
                cout << "-------------------------------" << endl;
                // receiving user input
                cout << "Please Enter a Celsius value: ";
                cin >> fahrenheitValue;

                // calling the function to do coversion
                celsiusValue = fahrenheitToCelsius(fahrenheitValue);
                cout << "\nFahrenheit value is : " << celsiusValue << "C" << endl;
            }
            break;


            // Calculate Circumference of a circle
            case 3:
            {
                cout << "\nCalculate Circumference of a circle" << endl;
                cout << "-------------------------------" << endl;
                // receiving user input
                cout << "\nPlease enter radius of circle: ";
                cin >> radiusValue;

                // Input Validation
                while(radiusValue < 0)
                {
                    cout << "\nError! Invalid option entered. Please enter again." << endl;
                    cout << "\nPlease enter radius of circle: ";
                    cin >> radiusValue;
                }

                //calling the function to do calculation
                circumferenceValue = calcCircumference(radiusValue);
                cout << "\nCircumference value of the circle is : " << circumferenceValue << endl;

            }
            break;


            //Calculate Area of a circle
            case 4:
            {
                cout << "\nCalculate Area of a circle" << endl;
                cout << "-------------------------------" << endl;
                // receiving user input
                cout << "\nPlease enter radius of circle: ";
                cin >> radiusValue;

                // Input Validation
                while(radiusValue < 0)
                {
                    cout << "\nError! Invalid option entered. Please enter again." << endl;
                    cout << "\nPlease enter radius of circle: ";
                    cin >> radiusValue;
                }

                // calling the function to do calculation
                areaValue = areaOfCircle(radiusValue);
                cout << "\nArea value of the circle is : " << areaValue << endl;
            }
            break;


             //Area of Rectangle
            case 5:
            {
                cout << "\nArea of Rectangle" << endl;
                cout << "-------------------------------" << endl;

                  // receiving user input

                cout << "\nEnter length of rectangle : ";
                cin >> length;

                // Input Validation
                while(length < 0)
                {
                    cout <<  "\nError! Please Enter Value greater than 1 !!!!" << endl;
                    cout << "\nPlease Enter length of rectangle : ";
                    cin >> length;
                }

                cout << "\nEnter breadth of rectangle : ";
                cin >> breadth;

                // Input Validation
                while(breadth < 0)
                {
                    cout <<  "\nError! Please Enter Value greater than 1 !!!!" << endl;
                    cout << "\nPlease Enter breadth of rectangle : ";
                    cin >> breadth;
                }

                  // Formula to calculate area of Rectangle
                  area = length * breadth;
                  cout << "\nArea of rectangle : " << area <<"\n" <<endl;

            }
            break;


            //Area of Triangle (Heron�s Formula)
            case 6:
            {
                cout << "\nArea of Triangle (Heron's Formula)" << endl;
                cout << "-------------------------------------" << endl;


                cout<<"\nInput the length of 1st side  of the triangle : ";
    	        cin>>side1;

                  // Input Validation
                while(side1 < 0)
                {
                    cout <<  "\nError! Please Enter Value greater than 1 !!!!" << endl;

                    cout<<"\nPlease Input the length of 1st side  of the triangle : ";
    	            cin>>side1;
                }

                cout<<"\nInput the length of 2nd side  of the triangle : ";
    	        cin>>side2;

                // Input Validation
                while(side2 < 0)
                {
                    cout << "\nError!!! Please Enter Value greater than 1 !!!!" << endl;
                    cout<<"\nPlease Input the length of 2nd side  of the triangle : ";
    	            cin>>side2;
                }


                cout<<"\nInput the length of 3rd side  of the triangle : ";
                cin>>side3;

                // Input Validation
                while(side3 < 0)
                {
                    cout <<  "\nError! Please Enter Value greater than 1 !!!!" << endl;
                    cout<<"\n Please Input the length of 3rd side  of the triangle : ";
                    cin>>side3;
                }

                  // Formula to calculate area of Rectangle
                  s = (side1+side2+side3)/2;

		      area1 = sqrt(s*(s-side1)*(s-side2)*(s-side3));

         cout<<"\nThe area of the triangle is (Heron's Formula): "<< area1 <<"\n" <<endl;
         cout << endl;


            }
            break;


            //Volume of Cylinder
            case 7:
            {
                cout << "\nVolume of Cylinder" << endl;
                cout << "-------------------------------" << endl;

                //receiving user input (height)
                cout << "Please Enter a height value: ";
                cin >> cylinderHeight;

                while(cylinderHeight < 1 || cin.fail()){
                    cout << "Please Enter value greater than 1!" << endl;
                if (cin.fail())
                    {
                        cin.clear();
                        cin.ignore(3000, '\n');
                    }
                    cout << "\nError! Please Enter value greater than 1!" << endl;
                    cout << "\nPlease Enter a height value: ";
                    cin >> cylinderHeight;
                    cin.ignore(3000, '\n');
                    }

                //receiving user input (radius)
                cout << "Please Enter a radius value: ";
                cin >> cylinderRadius;

                while(cylinderRadius< 1 || cin.fail()){
                    cout << "Please Enter value greater than 1!" << endl;
                    if (cin.fail())
                    {
                        cin.clear() ;
                        cin.ignore(3000, '\n');
                    }
                    cout << "\nError! Please Enter value greater than 1!" << endl;
                    cout << "\nPlease Enter a radius value: ";
                    cin >> cylinderRadius;
                    cin.ignore(3000, '\n');
                    }

                cylinderVolume = cylinderVolumeCalculation(cylinderHeight, cylinderRadius);

                cout << "The Volume of the Cylinder is: " << cylinderVolume << "m^3" << endl;


            }
            break;

            case 8:
            {
                cout << "\nVolume of Cone" << endl;
                cout << "-------------------------------" << endl;

                //receiving user input (height)
                cout << "Please Enter a height value: ";
                cin >> coneHeight;

                while(coneHeight < 1 || cin.fail()){
                    cout << "Please Enter value greater than 1!" << endl;
                if (cin.fail())
                    {
                        cin.clear();
                        cin.ignore(3000, '\n');
                    }
                    cout << "\nError! Please Enter value greater than 1!" << endl;
                    cout << "\nPlease Enter a height value: ";
                    cin >> coneHeight;
                    cin.ignore(3000, '\n');
                    }

                //receiving user input (radius)
                cout << "Please Enter a radius value: ";
                cin >> coneRadius;

                while(coneRadius< 1 || cin.fail()){
                    cout << "Please Enter value greater than 1!" << endl;
                    if (cin.fail())
                    {
                        cin.clear() ;
                        cin.ignore(3000, '\n');
                    }
                    cout << "\nError! Please Enter value greater than 1!" << endl;
                    cout << "\nPlease Enter a radius value: ";
                    cin >> coneRadius;
                    cin.ignore(3000, '\n');
                    }
                    coneVolume = coneVolumeCalculation(coneHeight, coneRadius);

                    cout << "The Volume of the Cone is: " << coneVolume << "m^3" << endl;

            }
            break;



            // quit program
            case 0:
            {
                cout << "\nTHANK YOU FOR USING OUR PROGRAM!" << endl;
                return 0;
            }

        }

    } while(option != 0);
}
// end of program

// function to convert from celsius to fahrenheit
double celsiusToFahrenheit(double celsius) {
    if (celsius == 0){
        return 32;
    }
    else
        return celsius * 33.8;
}

// function to convert from fahrenheit to celsius
double fahrenheitToCelsius(double farenheit) {
    if (farenheit == 0){
        return -17.7778;
    }
    else
        return farenheit * -17.2222;
}

// function to calculate circumference of a circle
double calcCircumference(double radius) {

    return 2 * 3.141 * radius;
}

// function to calculate area of a circle
double areaOfCircle(double radius){

    return 3.141 * radius * radius;
}

// function to calculate volume of cylinder
double cylinderVolumeCalculation(double height, double radius) {
        return (3.14*(radius*radius)*height);
}

// function to calculate volume of cone
double coneVolumeCalculation(double height, double radius) {
        return (3.14*(radius*radius)*(height/3));
}
// end
